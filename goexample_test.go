package goexample

import (
	"bytes"
	"testing"
)

func TestHello(t *testing.T) {
	var b bytes.Buffer

	input := "Tintin"

	err := Hello(&b, input)
	if err != nil {
		t.Error("unexpected error", err)
	}

	expected := "Hello Tintin!"
	actual := b.String()

	if expected != actual {
		t.Errorf("Hello(%v):\n\texpected %v\n\tactual   %v\n",
			input,
			expected,
			actual,
		)
	}
}
