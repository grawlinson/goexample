// Package goexample provides an example of how to use Gitlab CI/CD.
package goexample

import (
	"fmt"
	"io"
)

// Hello says Hello to the person whose name is passed to the function.
func Hello(w io.Writer, name string) error {
	_, err := fmt.Fprintf(w, "Hello %s!", name)
	return err
}
