# goexample

Package used to test GitLab CI/CD functionality.

The template for `.gitlab-ci.yml` can be found [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/678f55da3e608a1507e451e72540d63f3bacc315/lib/gitlab/ci/templates/Go.gitlab-ci.yml)

## Badges

### Gitlab CI

[![pipeline status](https://gitlab.com/grawlinson/goexample/badges/master/pipeline.svg)](https://gitlab.com/grawlinson/goexample/commits/master)

[![coverage report](https://gitlab.com/grawlinson/goexample/badges/master/coverage.svg)](https://gitlab.com/grawlinson/goexample/commits/master)

### Codecov

[![codecov](https://codecov.io/gl/grawlinson/goexample/branch/master/graph/badge.svg)](https://codecov.io/gl/grawlinson/goexample)

### Go Report Card

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/grawlinson/goexample)](https://goreportcard.com/report/gitlab.com/grawlinson/goexample)

### GoDoc

[![GoDoc](https://godoc.org/gitlab.com/grawlinson/goexample?status.svg)](https://godoc.org/gitlab.com/grawlinson/goexample)

