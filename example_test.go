// Package goexample implements a function that does absolutely nothing
// but say hello to users.
package goexample

import (
	"bytes"
	"fmt"
	"log"
)

func ExampleHello() {
	var b bytes.Buffer

	name := "Tintin"

	err := Hello(&b, name)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Printf("%v", b.String())
	// Output: Hello Tintin!
}
